const router = require('express').Router();
const Subscribe = require('../../models/subscription');
const Session= require('../../models/session');
const Methods = require('../../methods/custom');
const statusMsg = require('../../methods/statusMsg');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');

var multer  = require('multer');
var aws = require('aws-sdk');
var fs = require('fs');

/* Multer set storage location*/
var storage = multer.diskStorage({
	
	filename: function (req, file, cb) {
		   var str = file.originalname;
		   str = str.replace(/\s+/g, '-').toLowerCase();
		   global.poster = Date.now() + '_' + str;
		   cb(null, poster);
	 }
   });

   var upload = multer({ storage: storage });
   aws.config.update({ accessKeyId: config.ACCESS_KEY_ID, secretAccessKey: config.SECRET_ACCESS_KEY });
   aws.config.update({region: config.REGION});

// Delete image
var s3client = new aws.S3({
	accessKeyId: config.ACCESS_KEY_ID, secretAccessKey: config.SECRET_ACCESS_KEY, params: {
		Bucket: config.BUCKET_NAME,
	},
});


// Here the roo path '/' is  '/api/plans/'
router.get('/', (req, res) => {
	res.send('This is the api route for Subscription management');
});

router.get('/getallSubscribes', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";

					Subscribe.selectTable(table);
					Subscribe.query('basket')
					.then((subscribes) => {
						res.send(subscribes);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_store_data';
					const bodyParams = Methods.initializer(req, Subscribe);

					Subscribe.selectTable(table);
					Subscribe.getItem(bodyParams,{})
					.then((subscribe) => {
						res.send(subscribe);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/addSubscribe',upload.single('image'), (req, res) => {
	const token = req.body.token || req.headers['token'];
	const bodyParams=req.body;
	
	var s3 = new aws.S3();
	var hello=req.file.path;
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err)
				{
					res.send({
						status:"failure",
						message:"Invalid Token"
					});
				}
				else
				{
						Session.selectTable(decode.storeName+"_session_data");
						Session.getItem(decode.sessionId,{})
						.then((session) => {
							var subscription_ids=session.subscription_ids;
							subscription_ids.push(req.body.collectionId);
							var putparams={
								session_id:decode.sessionId,
								subscription_ids:subscription_ids
							}
							Session.updateItem(putparams,{})
							.then((updatedSession) => {
								s3.upload({
									"ACL":"public-read",
									"Bucket": config.BUCKET_NAME,
									"Key": poster,
									"Body": fs.createReadStream(hello),
									ContentType : "image/jpeg"
								}, function(err, data) {
									if (err) {
										console.log("Error uploading data: ", err);
									}else {
										console.log("upload success");
										const imageurl="https://s3-ap-southeast-1.amazonaws.com/"+config.BUCKET_NAME+"/"+poster;
										//const bodyParams = Methods.initializer(req, Subscribe);
										const putParams = {
											"collectionType":"basket",
											"collectionId": req.body.collectionId,
											"title": req.body.title,
											"description": req.body.description,
											"srp": req.body.srp,
											"imageURL":imageurl,
											"frequency": req.body.frequency,
											"period": req.body.period,
											"subscriptionProducts":req.body.subscriptionProducts,
											"featuredSubscription":req.body.featuredSubscription,
											"shipping_dicount":req.body.shipping_discount,
											"trail_discount":req.body.trail_discount,
											"gifting_discount":req.body.gifting_discount
											
										};
											Subscribe.createItem(putParams, {
												table: decode.storeName+"_store_data",
												overwrite: false
											}).then((subscribe) => {
												res.send({
													status:"success",
													message:"subscription added successfully"
												});
											}).catch((err) => {
												res.send(statusMsg.errorResponse(err));
											})
											
									}
								});

							}).catch((err) => {
								console.log("error while updating the session with products::"+err);
								res.send({status:"failure",message:"error while updating session with product"});
							})

						}).catch((err) => {
							console.log("Error while getting an seesion item::"+ err);
							res.send({status:"error",message:"get session item error!"});
						})
				}
			});
		}else {
        	res.send({status:"failure",message:"please send a token"});
	   }
});

router.get('/editsubscribe/:type/:id', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = `${decode.storeName}_store_data`;
                    const params={
						"collectionType":req.params.type,
						"collectionId":req.params.id
					}

                    Subscribe.selectTable(table);
					Subscribe.getItem(params, {})
					.then((subscribe) => {
						res.send(subscribe);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}	
});

/*router.put('/updateSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams= req.body;
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				if(req.file == undefined){
					console.log(req.body);
					var updateParams = {
						collectionType:req.body.collectionType,
						collectionId: req.body.collectionId,
						title:req.body.title,
						description:req.body.description,
						srp: req.body.srp,
						frequency: req.body.frequency,
						expiryDate: req.body.expiryDate,
						subscriptionProducts:req.body.subscriptionProducts,
						featuredSubscription:req.body.featuredSubscription,
						shippingFree:req.body.shippingFree,
						freeTrial:req.body.freeTrial,
						freeTrailDays:req.body.freeTrailDays
					};
					console.log("Subscribe:::"+JSON.stringify(updateParams));
					const table = decode.storeName+"_store_data";
				    Subscribe.selectTable(table);
					Subscribe.updateItem(updateParams, {}, (err, data) => {
						if (err) {
							res.send(statusMsg.errorResponse(err));
						}
						else{
							res.json(data);
						}
				   });
				}else{
					console.log("req value: "+JSON.stringify(req.file));
					//console.log("image:"+req.file.originalname);
					var s3 = new aws.S3();
					var hello=req.file.path;
					s3.upload({
						"ACL": "public-read",
						"Bucket": "cadenza-images",
						"Key": poster,
						"Body": fs.createReadStream(hello),
						ContentType: "image/jpeg"
								
						}, function(err, data) {
						if (data) {
							console.log("upload success");
							const imageurl="https://s3.ap-south-1.amazonaws.com/cadenza-images/"+poster;
							var updateParams = {
								collectionType:req.body.collectionType,
								collectionId: req.body.collectionId,
								title:req.body.title,
								description:req.body.description,
								srp: req.body.srp,
								frequency: req.body.frequency,
								expiryDate: req.body.expiryDate,
								subscriptionProducts:req.body.subscriptionProducts,
								featuredSubscription:req.body.featuredSubscription,
								shippingFree:req.body.shippingFree,
								freeTrial:req.body.freeTrial,
								freeTrailDays:req.body.freeTrailDays,
								imageURL:imageurl
							};

				const table = decode.storeName+"_store_data";
				Subscribe.selectTable(table);
				var putparams={
					"collectionType":'basket',
					"collectionId":req.body.collectionId,
					"plans":bodyParams.plans
				}
				Subscribe.updateItem(putparams,{},(err,data)=>{
					if(err)
					{
						res.send(statusMsg.errorResponse(err));
					}
					else{
						console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
						res.send({
							status:"success",
							message:"Updated baskets with plans successfully"
						});
					}
				});
				//apiOperation(req, res, Subscribe.updateItem);
							console.log("Subscribe1:::"+JSON.stringify(updateParams));
							const table = `${decode.storeName}_store_data`;
							Subscribe.selectTable(table);
							Subscribe.updateItem(updateParams, {}, (err, data) => {
								if (err) {
									res.send(statusMsg.errorResponse(err));
								}
								else{
									console.log("updated");
									var s3client = new aws.S3({
										accessKeyId: 'AKIAIJCZVHKXQKI3ENYQ', secretAccessKey: '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', params: {
											Bucket: 'monthlify.in',
										},
									});
									const url=req.body.preImage;
									const Imagename=url.substring(url.lastIndexOf('/')+1);
									console.log("Image"+Imagename);
									s3client.deleteObject({
										Key: Imagename,
									}, function (err, data) {
										if (err) {
											console.log("Error deleting data: ", err);
										}
										else {
											
											console.log("deleted:" +JSON.stringify(data));
											res.send("Subscribe updated successfully");
										}
				
									})

								}
							});
							
						}
					});
				}
				   
            }
        })
    }
    else{
        res.send("please send a token");
    }
});*/
router.put('/updateSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];
	var bodyParams= req.body;
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}else {
				const table = decode.storeName+"_store_data";
				var putparams={
					"collectionType":'basket',
					"collectionId":req.body.collectionId,
					"plans":bodyParams.plans
				}

				Subscribe.selectTable(table);
				Subscribe.updateItem(putparams,{})
				.then((data) => {
					res.send({
						status:"success",
						message:"Updated baskets with plans successfully"
					});
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.delete('/deleteSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";
					const removeCallback = (err, removeData) => {
						
						const url = req.body.preImage;
						const Imagename = url.substring(url.lastIndexOf('/')+1);

						s3client.deleteObject({
							Key: Imagename,
						}, function (err, data) {
							if (err) {
								console.log("Error deleting data: ", err);
								res.send("Error deleting data: ", err);
							}
							else {
								res.send("Subscription deleted successfully");
							}
	
						})
					}
					// passing the conditional object here {} as second parameter
					Subscribe.selectTable(table);
					Subscribe.deleteItem({
						collectionType: req.body.collectionType,
						collectionId: req.body.collectionId
					}, {ReturnValues: 'ALL_OLD'} ,removeCallback);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});


// Frontend endpoints
router.post('/getbaskets', (req, res) => {
	
	const storeName = req.body.storeName;
	const table = storeName+"_store_data";

	Subscribe.selectTable(table);
	Subscribe.query('basket')
	.then((subscribes) => {
		res.json(subscribes);
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err))
	})
})

router.post('/getplans', (req, res) => {
	const params = {
		"collectionType":"basket",
		"collectionId":req.body.Id
	}
	const table = req.body.storeName+"_store_data";

	Subscribe.selectTable(table);
	Subscribe.getItem(params, {})
	.then((basket) => {
		res.json(basket);
	}).catch((err) => {
		res.send(statusMsg.errorResponse(err))
	})
})

module.exports = router;
