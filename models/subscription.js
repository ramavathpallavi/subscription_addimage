const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['title', 'description','imageURL','frequency', 'expiryDate','subscriptionProducts','featuredSubscription','shippingFree','freeTrial','plans'];

const subscribeSchema = {
    hashKey: 'collectionType',
    rangeKey: 'collectionId',
    timestamps: true,
    schema: Joi.object({
        collectionType: 'basket', 
        collectionId: Joi.string().alphanum(),
        title: Joi.string(),
        description: Joi.string(),
        srp: Joi.number().positive().min(1),
        imageURL: Joi.string(),
        frequency: Joi.number().min(1),
        expiryDate: Joi.string(),
        subscriptionProducts:Joi.object(),
        featuredSubscription:Joi.string(),
        shippingFree:Joi.string(),
        freeTrial:Joi.string(),
	plans:Joi.object(),
    }).optionalKeys(optionalParams).unknown(true)
};

const attToGet = ['title', 'description', 'price', 'imageURL','frequency', 'expiryDate','subscriptionProducts','featuredSubscription','shippingFree','freeTrial','plans'];
const attToQuery = ['title', 'description', 'price', 'imageURL','frequency', 'expiryDate','subscriptionProducts','featuredSubscription','shippingFree','freeTrial', 'collectionId','collectionType','plans'];

const optionsObj = {
    attToGet,
    attToQuery,
};

const Subscribe = SchemaModel(subscribeSchema, optionsObj);

module.exports = Subscribe;
